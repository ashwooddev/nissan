### TODO:
[ ] 2h. - element `range`
[x] `car-more` page
[x] replace `main slider` to `banner` on other pages
[x] create element `table`
[x] `showcase` on `car-more` page
[ ] sort-filter arows `js`
[ ] fix header on < 775px and < 998px

---

##### on mobile
[ ] 30min. - car review page `table` header fix
[x] hide `main slider` controls
[x] remove header and text on `main slider`
[x] work on sorting `dropdown elements` in `gray-box`

---

#### refactor, other
[x] refactor code on `car-more` page
[ ] make page with all elements

---

#### new fixes
[ ] 30min. - квадрат с лупой или галкой в зависимоти от параметра на  `card`
[ ] 30min. - плашка на странице `подробнее о машине` "Одобрено Ниссан проверено"
[ ] 30min. - информ бокс серого цвета `Примем Ваш автомобиль в зачет.` + красным цвнтом `Автомобиль можно купить в кредит`
[ ] 30min. - большой блок на всю ширену страницы `Остались вопросы?` на странице `подробнее о машине`

##### [ ] 1h. - форма `Отправить заявку` состоит из полей:
0. поле ввода `ФИО`
0. поле ввода `Ваш телефон*`
0. поле ввода `e-mail`
0. поле ввода `Тема письма`
0. textarea `Комментарий`
0. checkbox `При отправке заявки вы соглашаетесь на использование ваших персональных данных` ссылка: `использование ваших персональных данных`
0. кнопка `Отправить`
