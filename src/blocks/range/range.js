$(".range__body__scroll-click-area").on('click', function(e) {

    let body = $(e.target).parent().parent();

    let w = e.target.clientWidth;
    let proc = e.target.clientWidth / 100;
    let start = body.find("input.range__body__value-from");
    let end = body.find("input.range__body__value-to");

    let x = body.data("val_start");
    let y = body.data("val_end");
    let z = y - x;
    let zproc = z / 100;
    let zcurr = (x + zproc * (e.offsetX / proc)).toFixed();

    if (e.offsetX < e.target.clientWidth / 2) {
        let left = e.offsetX / proc;
        body.find(".range__body__scroll__from").css("left", left + "%");
        start.val(zcurr);
        body.find(".range__body__scroll-bg").css("left", left + "%");
    } else {
        let right = (e.target.clientWidth - e.offsetX) / proc;
        body.find(".range__body__scroll__to").css("right", right + '%');
        end.val(zcurr);
        body.find(".range__body__scroll-bg").css("right", right + '%');
    }
});

$(".range__body__value-from-to input").on('keyup', function(e) {
    let body = $(e.target).parent().parent();

    let x = body.data("val_start");
    let y = body.data("val_end");

    $(e.target).val($(e.target).val().replace(/[^0-9]/g, ""));
});
