if($('.dropdown').length > 0) { /* dropdown */
	$( '.dropdown__title' ).on('click', function (e) {
		if($( e.target ).parent().attr('data-toggle') === 'close') {
			$( e.target ).parent().attr('data-toggle', 'open');
			$( e.target ).parent().removeClass('close');
		} else {
			$( e.target ).parent().attr('data-toggle', 'close');
			$( e.target ).parent().addClass('close');
		}
	});

	$( '.dropdown__list > ul > li' ).on('click', function (e) {
		$('#' + $(e.target).parent().parent().parent().attr('id') + ' .dropdown__title').val($(e.target).text()).css('color', '#000000');
		$( $(e.target).parent().parent().parent() ).attr('data-toggle', 'close');
		$( $(e.target).parent().parent().parent() ).addClass('close');
	});
	window.setInterval(function() {
		$( '.dropdown__list > ul' ).css('width', $( '.dropdown__title' ).css('width'));
	}, 200);
/* dropdown */ }
