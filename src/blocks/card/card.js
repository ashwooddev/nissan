if($( '.card' ).length > 0) { /* cards */
	let cards = $( '.card' );
	for(let j = 0; j < cards.length; j++) {
		let imgs = $( cards[j] ).find( '.card__header-imgs > img' );
		let ul = document.createElement( 'ul' );
		let li = [];
		for (let i = 0; i < imgs.length; i++) {
			let element = document.createElement( 'li' );
			if( $( imgs[i] ).hasClass( 'active' ) ) {
				$( element ).addClass( 'active' );
			}
			$( element ).attr('data-item', i);
			li.push( element );
		}
		$( ul ).html( li.slice() );
		let pagination = $( cards[j] ).find( '.card__header-pagination' );
		$( pagination ).html( ul );
		$( pagination ).find( 'li' ).hover(function(e) {
			let images = $( e.target ).parent().parent().parent().parent().find( '.card__header-imgs > img' );
			for (let i = 0; i < images.length; i++)
				if( $( images[i] ).hasClass( 'active' ))
					if(i !== $( e.target ).attr( 'data-item' )) {
						$( images[i] ).removeClass( 'active' );
						$( e.target ).parent().parent().parent().parent().find('ul > li.active').removeClass( 'active' );
						$( images[parseInt($( e.target ).attr('data-item'))] ).addClass( 'active' );
						$( e.target ).addClass( 'active' );
					}
		});
	}
 /* cards */ }
