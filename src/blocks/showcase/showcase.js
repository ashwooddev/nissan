if($('.showcase').length > 0) { /* showcase */
	let ID = $( '.showcase__pagination' ).attr('data-car-id'),
			prefix = $( '.showcase__pagination' ).attr('data-img-names').split(','),
			format = $( '.showcase__pagination' ).attr('data-img-format');
	$( '.showcase__main-box' ).css('background-image', 'url(../img/' + ID + '_0_b.' + format + ')');

	for(key in prefix) {
		let div = document.createElement('div');
		if(parseInt(key) === 0) {
			$( div ).addClass('active');
			$( '.showcase__main-box' ).css('background-image', 'url(../img/' + ID + '_' + prefix[key] + '_b.' + format + ')');
			$( '.showcase__main-box' ).fadeIn(650, function(){});
		}
		$( div ).attr('data-img-url', ID + '_' + prefix[key] + '_b.' + format)
						.css('background-image', 'url(../img/' + ID + '_' + prefix[key] + '.' + format + ')');
		document.querySelector( '.showcase__pagination' ).innerHTML += div.outerHTML;
	}

	$( '.showcase__pagination div' ).hover(function(e) {
		$( e.target ).parent().find('.active').removeClass('active');
		$( e.target ).addClass('active');
		$( '.showcase__main-box' ).fadeOut(650, function(){
			$( '.showcase__main-box' ).css('background-image', 'url(../img/' + $( e.target ).attr('data-img-url') + ')');
			$( '.showcase__main-box' ).fadeIn(650, function(){});
		});
	}, function(){});
/* showcase */ }
