if($('ul.menu').length > 0) { /* menu */
	let button = $( '.burger' );
	button.on('click', () => {
		if($( 'ul.menu' ).hasClass('show'))
			$( 'ul.menu' ).fadeOut(350, function() { $( 'ul.menu' ).removeClass('show') });
		else
			$( 'ul.menu' ).fadeIn(350, function() { $( 'ul.menu' ).addClass('show') });
	});
/* menu */ }
