// var nissanSiema = new Siema({
// 	selector: "#slider > .siema",
// 	duration: 700,
// 	easing: "ease-in-out",
// 	perPage: 1,
// 	startIndex: 0,
// 	draggable: true,
// 	multipleDrag: true,
// 	threshold: 20,
// 	loop: true,
// 	rtl: false,
// 	onInit: () => {},
// 	onChange: () => {}
// });
// document.querySelector("#slider__controls-prev").addEventListener("click", () => nissanSiema.prev());
// document.querySelector("#slider__controls-next").addEventListener("click", () => nissanSiema.next());

var nissanSlider = Peppermint(document.getElementById("peppermint"), {
  dots: false,
  slideshow: true,
  speed: 1000,
  slideshowInterval: 2000,
  stopSlideshowAfterInteraction: false,
  disableIfOneSlide: true,
  onSetup: function(n) {
    console.log("Peppermint setup done. Slides found: " + n);
  }
});
document.querySelector("#slider__controls-prev").addEventListener("click", () => nissanSlider.prev());
document.querySelector("#slider__controls-next").addEventListener("click", () => nissanSlider.next());